from keras.preprocessing.image import ImageDataGenerator, array_to_img, img_to_array, load_img
import os
import time
from tqdm import tqdm
import shutil

import numpy as np
#import matplotlib.pyplot as plt

import cv2

datagen = ImageDataGenerator(
        rotation_range=25,
        width_shift_range=0.2,
        height_shift_range=0.2,
        shear_range=0.2,
        zoom_range=0.2,
        fill_mode='nearest')

path1="/home/xfce/datset1/"

os.chdir("/home/xfce/datset1/")
leng=os.listdir(".")
print(leng)


for folder in tqdm(leng,desc="loop1"):
  newPath=path1+folder+"/"
  os.chdir(newPath)
  imgpath=os.listdir(".")
  for img in imgpath:
    img_path=newPath+img
    if img_path is not "genDir":
      img = load_img(img_path)  
      x = img_to_array(img)  
      x = x.reshape((1,) + x.shape)  


    i = 1
    if "genDir" not in os.listdir("."):
    
      os.mkdir("genDir")
    #print(img_path)
    dir=newPath+"genDir"
    for batch in datagen.flow(x, batch_size=1,save_to_dir=dir,save_prefix='genImage',save_format='png'):#save_to_dir=dir,,save_prefix='cat'
        i += 1
        if i > 15:
            break 
  #break



import os
def checkNoIM(path):
  d=[]
  l=os.listdir(path)
  for fold in l:
    newpth=path1+fold+"/"
    lgth=len(os.listdir(newpth))
    d.append(lgth)
  return d

print(checkNoIM(path1))