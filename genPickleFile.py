import os
import cv2
import numpy as np
from tqdm import tqdm
import time


path="/home/bspwm/CNNpytorch/pytorchFinalScripts/numbers/"
st=os.listdir(path)
st.sort()

IMG_SIZE = 100
num_classes=50
LABELS={}

training_data = []


i=0
for fold in st:
    LABELS[fold]=i
    i=i+1


for lab in LABELS:
    print(path+lab)
    break


def make_training_data():
    for label in LABELS:
            #print(label)
        for f in tqdm(os.listdir(path+label)):
                
            
            path1 = os.path.join(path+label, f)

            img = cv2.imread(path1, cv2.IMREAD_GRAYSCALE)
            img = cv2.resize(img, (IMG_SIZE,IMG_SIZE))
            #print(img)
            training_data.append([np.array(img), np.eye(num_classes)[LABELS[label]]])  



    np.random.shuffle(training_data)
    np.save("numbers.npy",training_data)



make_training_data()

