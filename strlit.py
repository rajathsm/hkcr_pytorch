#import streamlit as st
#from PIL import Image

#uploaded_file = st.file_uploader("Choose a file", type="png")

#if uploaded_file is not None:
	#image = Image.open(uploaded_file)
	#image.save("savedim.png")
	#st.write(image)

	
#pip install streamlit pillow opencv-python numpy

import streamlit as st
from PIL import Image

import os
import cv2
import tensorflow as tf
import json
import numpy as np





def prepare(filepath):
	IMG_SIZE = 100  
	img_array = cv2.imread(filepath, cv2.IMREAD_GRAYSCALE)
	new_array = cv2.resize(img_array, (IMG_SIZE, IMG_SIZE))
	return new_array.reshape(-1, IMG_SIZE, IMG_SIZE, 1)
#pat="/home/xfce/CNN/extra/"
mod="/home/bspwm/Documents/Str/CnnModels/new/"
act=os.listdir(mod)
activity = act
choice = st.sidebar.selectbox("Select model",activity)


model = tf.keras.models.load_model(mod+choice)

st.header('Handwritten Kannada Character Recognition')

#st.write("loaded model:",choice)

if choice=="New1_1(1).model":
	cat=['Sample001', 'Sample002', 'Sample003', 'Sample004', 'Sample005', 'Sample006', 'Sample007', 'Sample008', 'Sample009', 'Sample010', 'Sample011', 'Sample012', 'Sample013', 'Sample014', 'Sample015', 'Sample016', 'Sample018', 'Sample035', 'Sample052', 'Sample069', 'Sample086', 'Sample103', 'Sample120', 'Sample137', 'Sample154', 'Sample171', 'Sample188', 'Sample205', 'Sample222', 'Sample239', 'Sample256', 'Sample273', 'Sample290', 'Sample307', 'Sample324', 'Sample341', 'Sample358', 'Sample375', 'Sample392', 'Sample409', 'Sample426', 'Sample443', 'Sample460', 'Sample494', 'Sample511', 'Sample545', 'Sample562', 'Sample579', 'Sample596', 'Sample613']

if choice=="numbers.h5" or choice=="numbers_96-5.h5":
	cat=['Sample648', 'Sample649', 'Sample650', 'Sample651', 'Sample652', 'Sample653', 'Sample654', 'Sample655', 'Sample656', 'Sample657']	
	

uploaded_file = st.file_uploader("Choose a file", type="png")
if uploaded_file is not None:
	st.image(uploaded_file,width=300)





pred=""
ind=0

v=['&#3205;', '&#3206;', '&#3207;', '&#3208;', '&#3209;', '&#3210;', '&#3211;', '&#3296;', '&#3214;', '&#3215;', '&#3216;', '&#3218;', '&#3219;', '&#3220;', '&#3202;', '&#3203;', '&#3221;', '&#3222;', '&#3223;', '&#3224;', '&#3225;', '&#3226;', '&#3227;', '&#3228;', '&#3229;', '&#3230;', '&#3231;', '&#3232;', '&#3233;', '&#3234;', '&#3235;', '&#3236;', '&#3237;', '&#3238;', '&#3239;', '&#3240;', '&#3242;', '&#3243;', '&#3244;', '&#3245;', '&#3246;', '&#3247;', '&#3248;', '&#3250;', '&#3251;', '&#3253;', '&#3254;', '&#3255;', '&#3256;', '&#3257;']



if uploaded_file is not None:

	if st.button('Predict'):

		if uploaded_file is not None:
			image = Image.open(uploaded_file)
			image.save("savedim.png")
			#st.write(image)
			with st.spinner('predicting...'):
				prediction = model.predict([prepare("savedim.png")])
				print(prediction)
				idxs = np.array(prediction)[::-1][:2]
				print(idxs)
				ind=np.argmax(prediction[0])
				pred=str(cat[ind])
			
			
			st.write("Predicted output is:",pred)
			#print(pred,ind,str(v[ind]))
			chara=str(v[ind])	
			html_code="<h1>" + "Predicted Charcter: "+ chara + "</h1>"
			st.markdown(html_code, unsafe_allow_html=True)
			
				
			
			import matplotlib.pyplot as plt

			import numpy as np

			r=['Sample001', 'Sample002', 'Sample003', 'Sample004', 'Sample005', 'Sample006', 'Sample007', 'Sample008', 'Sample009', 'Sample010', 'Sample011', 'Sample012', 'Sample013', 'Sample014', 'Sample015', 'Sample016', 'Sample018', 'Sample035', 'Sample052', 'Sample069', 'Sample086', 'Sample103', 'Sample120', 'Sample137', 'Sample154', 'Sample171', 'Sample188', 'Sample205', 'Sample222', 'Sample239', 'Sample256', 'Sample273', 'Sample290', 'Sample307', 'Sample324', 'Sample341', 'Sample358', 'Sample375', 'Sample392', 'Sample409', 'Sample426', 'Sample443', 'Sample460', 'Sample494', 'Sample511', 'Sample545', 'Sample562', 'Sample579', 'Sample596', 'Sample613']

			x=[]

			#for i in r:
			#	x.append(i.replace(i[:6],"sampl"))
			t=['ಅ', 'ಆ', 'ಇ', 'ಈ', 'ಉ', 'ಊ', 'ಋ', 'ೠ', 'ಎ', 'ಏ', 'ಐ', 'ಒ', 'ಓ', 'ಔ', 'ಅಂ', 'ಅಃ', 'ಕ', 'ಖ', 'ಗ', 'ಘ', 'ಙ', 'ಚ', 'ಛ', 'ಜ', 'ಝ', 'ಞ', 'ಟ', 'ಠ', 'ಡ', 'ಢ', 'ಣ', 'ತ', 'ಥ', 'ದ', 'ಧ', 'ನ', 'ಪ', 'ಫ', 'ಬ', 'ಭ', 'ಮ', 'ಯ', 'ರ', 'ಲ', 'ಳ', 'ವ', 'ಶ', 'ಷ', 'ಸ', 'ಹ']
			#x=v
			
			for i in range(51):
				if i==0:
					continue
				else:
					x.append("Folder"+str(i))

			y=[]
			for i in range(50):
				y.append(int(0))
			y[ind]=1

			indi=ind
			f=x[indi-3:indi]
			g=x[indi:indi+3]

			f.extend(g)

			f1=y[indi-3:indi]
			g1=y[indi:indi+3]

			f1.extend(g1)


			fig, ax = plt.subplots()    
			width = 0.5 # the width of the bars 
			ind = np.arange(len(f1))  # the x locations for the groups
			ax.barh(ind, f1, width, color="blue")
			ax.set_yticks(ind+width/2)
			ax.set_yticklabels(f, minor=False)
			plt.title('Prediction')
			plt.xlabel('probabilities')
			#plt.ylabel('y')      
			#plt.show()

			st.pyplot()
			#style=\"font-size:50px\"
			

			st.write("Mapping of labels to html character code")
			disD={}
			for fold,unic in zip(x,t):
				disD[fold]=unic
			st.json(disD)
				

			#st.success('Done!')




