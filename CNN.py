import torch

if torch.cuda.is_available():
    device = torch.device("cuda:0")  
    print("Running on the GPU")
else:
    device = torch.device("cpu")
    print("Running on the CPU")



import torch.nn as nn
import torch.nn.functional as F

class Net(nn.Module):
    def __init__(self):
        super().__init__()
        # self.conv1 = nn.Conv2d(1, 128, 5) # input is 1 image, 32 output channels, 5x5 kernel / window
        # self.conv2 = nn.Conv2d(128, 256, 5) # input is 32, bc the first layer output 32. Then we say the output will be 64 channels, 5x5 kernel / window
        # self.conv3 = nn.Conv2d(256, 512, 5)
        self.conv1 = nn.Conv2d(1, 64, 5) # input is 1 image, 64 output channels, 5x5 kernel / window
        self.conv2 = nn.Conv2d(64, 90, 5) # input is 32, bc the first layer output 32. Then we say the output will be 128 channels, 5x5 kernel / window
        self.conv3 = nn.Conv2d(90, 90, 5)

        self.m1 = nn.Dropout(p=0.4)
        self.m2 = nn.Dropout(p=0.3)
        self.m3 = nn.Dropout(p=0.3)


        #self.conv4 = nn.Conv2d(128, 128, 5)

        x = torch.randn(100,100).view(-1,1,100,100)
        self._to_linear = None
        self.convs(x)
        #print(self._to_linear)

        self.fc1 = nn.Linear(self._to_linear,140) #flattening.
        self.fc2 = nn.Linear(140, 64)
        self.fc3 = nn.Linear(64, 64)
        self.fc4 = nn.Linear(64, 50) # 512 in, 2 out bc we're doing 2 classes (dog vs cat).

    def convs(self, x):
        # max pooling over 2x2
        x = F.max_pool2d(F.relu(self.conv1(x)), (2, 2))
        x = F.max_pool2d(F.relu(self.conv2(x)), (2, 2))
        x = F.max_pool2d(F.relu(self.conv3(x)), (2, 2))


        if self._to_linear is None:
            self._to_linear = x[0].shape[0]*x[0].shape[1]*x[0].shape[2]
        return x

    def forward(self, x):
        x = self.convs(x)
        x = x.view(-1, self._to_linear) 
        x = F.relu(self.fc1(x))
        x=self.m1(x)
        x = F.relu(self.fc2(x))
        x=self.m2(x)
        x = F.relu(self.fc3(x))
        x=self.m3(x)
        x = self.fc4(x) 
        return F.softmax(x, dim=1)


net = Net()
net.to(device)
#print(net)
from torchsummary import summary
summary(net, input_size=(1, 100, 100))


import torch.optim as optim
optimizer = optim.Adam(net.parameters(), lr=0.001)
loss_function = nn.MSELoss()

#storing reference to training data
import numpy as np
training_data = np.load("/content/training_data57.npy", allow_pickle=True)
X = torch.Tensor([i[0] for i in training_data]).view(-1,100,100)
X = X/255.0
y = torch.Tensor([i[1] for i in training_data])

#storing reference to testing data
VAL_PCT = 0.2 
val_size = int(len(X)*VAL_PCT)
print(val_size)

train_X = X[:-val_size]
train_y = y[:-val_size]

test_X = X[-val_size:]
test_y = y[-val_size:]
print(len(train_X), len(test_X))



#training of NN
BATCH_SIZE = 100
EPOCHS = 50
from tqdm import tqdm
losses = []
ep=[]
acc=[]
step=[]
correct=0
total=0

for epoch in range(EPOCHS):
    running_loss = 0.0
    for i in tqdm(range(0, len(train_X), BATCH_SIZE)): # from 0, to the len of x, stepping BATCH_SIZE at a time. [:50] ..for now just to dev
        #print(f"{i}:{i+BATCH_SIZE}")
        batch_X = train_X[i:i+BATCH_SIZE].view(-1, 1, 100, 100)
        batch_y = train_y[i:i+BATCH_SIZE]
        batch_X, batch_y = batch_X.to(device), batch_y.to(device)

        net.zero_grad()

        outputs = net(batch_X)

        real_class = torch.argmax(train_y[i:i+BATCH_SIZE][0])
        #print("R",real_class)

        net_out = net(train_X[i].view(-1, 1, 100, 100).to(device))[0]  # returns a list, 
        #loss = loss_function(net_out, real_class)
        predicted_class = torch.argmax(net_out)
        #print("P",predicted_class)
        #f.append(loss)

        if predicted_class == real_class:
            correct += 1
        total += 1
        if i%1390==0:
          print("Accuracy: ", round(correct/total, 3))
        acc.append(round(correct/total, 3))
        step.append(i)
        loss = loss_function(outputs, batch_y)
        loss.backward()
        optimizer.step()    # Does the update
        running_loss += loss.item() * batch_X.size(0) 
    epoch_loss = running_loss / len(train_X)
    losses.append(epoch_loss)
    ep.append(epoch)

    print(f"Epoch: {epoch}. Loss: {loss}")
torch.save(net,"netModel.pth")



#calcuating the testing accuracy

correct = 0
total = 0
f=[]
with torch.no_grad():
    for i in tqdm(range(len(test_X))):
        real_class = torch.argmax(test_y[i]).to(device)
        net_out = net(test_X[i].view(-1, 1, 100, 100).to(device))[0]  # returns a list, 
        
        predicted_class = torch.argmax(net_out)
        

        if predicted_class == real_class:
            correct += 1
        total += 1
        f.append(round(correct/total, 3))
print("Accuracy: ", round(correct/total, 3))