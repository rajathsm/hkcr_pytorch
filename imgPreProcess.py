from PIL import Image, ImageChops, ImageOps, ImageFilter
import PIL
import os
import shutil
from tqdm import tqdm


def crop(image,name):
    img = Image.open(image)
    bg = Image.new(img.mode, img.size, img.getpixel((0, 0)))
    diff = ImageChops.difference(img, bg)
    diff = ImageChops.add(diff, diff, 2.0, -100)
    bbox = diff.getbbox()
    if bbox:
        img = img.crop(bbox)
        padding(img,name)

        #img.save("gen.png")

def padding(old_im,name):
    #old_im = Image.open(image)
    old_size = old_im.size
    new_size = ((old_size[0]+50), (old_size[1]+50))
    new_im = Image.new("RGB", new_size)
    new_im = ImageOps.invert(new_im)
    new_im.paste(old_im, (int((new_size[0]-old_size[0])/2),
                          int((new_size[1]-old_size[1])/2)))
    #new_im.save("pad.png")
    size208(new_im,name)

def size208(im,name):
    desired_size = 100
    
    old_size = im.size
    ratio = float(100)/max(old_size)
    new_size = tuple([int(x*ratio) for x in old_size])
    im = im.resize(new_size, Image.ANTIALIAS)
    new_im = Image.new("RGB", (100, 100))
    new_im = PIL.ImageOps.invert(new_im)
    new_im.paste(im, ((100-new_size[0])//2, (100-new_size[1])//2))
    new_im.save(name)



path="/home/bspwm/CNNpytorch/test/"

for fold in tqdm(os.listdir(path)):
    np=path+fold+"/"
    os.chdir(np)
    for imgName in os.listdir(np):
        if fold+"dir" not in os.listdir(np):
            os.mkdir(fold+"dir")
        if imgName != "dir":
            #os.mkdir("dir")
            os.chdir(np+fold+"dir")
            crop(np+imgName,imgName)

